LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := RemovePkgs
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_OVERRIDES_PACKAGES := AmbientSensePrebuilt AppDirectedSMSService ConnMO DCMO
LOCAL_OVERRIDES_PACKAGES += DMService GCS MaestroPrebuilt MicropaperPrebuilt MyVerizonServices
LOCAL_OVERRIDES_PACKAGES += YouTubeMusicPrebuilt SafetyHubPrebuilt SCONE ScribePrebuilt Showcase
LOCAL_OVERRIDES_PACKAGES += SprintDM SprintHM TurboPrebuilt Tycho USCCDM VZWAPNLib
LOCAL_OVERRIDES_PACKAGES += VzwOmaTrigger OBDM_Permissions obdm_stub arcore DevicePolicyPrebuilt
LOCAL_UNINSTALLABLE_MODULE := true
LOCAL_CERTIFICATE := platform
LOCAL_SRC_FILES := /dev/null
include $(BUILD_PREBUILT)
